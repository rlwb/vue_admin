export default {
  //时间戳转化为年月日
  dealDate: (date) => {
    const val = new Date(date);
    return `${val.getFullYear()}-${
      val.getMonth() + 1 > 10 ? val.getMonth() + 1 : "0" + (val.getMonth() + 1)
    }-${val.getDate()}`;
  },
};
