import axios from "axios";
import { Message } from "element-ui";
import { SET_TOKEN } from "@/store/mutation-type";
import router from "../router";
import store from "../store";
//serve process.env.NODE_ENV = "development"
//build process.env.NODE_ENV = 'production'
const http = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
});
// 添加请求拦截器
http.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    if (config.url !== "login") {
      config.headers.Authorization = store.state.token;
    }
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
http.interceptors.response.use(
  function (res) {
    // 对响应数据做点什么
    if (res.status >= 200 && res.status < 300) {
      //http状态码
      if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
        //后台接口定义的状态码
        return res.data;
      } else {
        if (res.data.meta.msg === "无效token") {
          router.replace({
            name: "Login",
          });
          store.commit(SET_TOKEN, "");
        }
        Message.error(res.data.meta.msg);
        return Promise.reject(res.data.meta.msg);
      }
    } else {
      Message.error(res.statusText);
      return Promise.reject(res.statusText);
    }
  },
  function (error) {
    // 对响应错误做点什么
    Message.error(error);
    return Promise.reject(error);
  }
);

export default http;
