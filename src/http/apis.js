import http from "./http";
//获取菜单
export const getMenus = () =>
  http({
    url: "menus",
    method: "GET",
  });
//登录
export const login = (data) =>
  http({
    url: "login",
    method: "post",
    data,
  });
//获取用户列表
export const getUserList = (params) =>
  http({
    url: "users",
    method: "get",
    params,
  });

//添加用户
export const addUser = (data) => {
  return http({
    url: "users",
    method: "post",
    data,
  });
};
//修改用户状态
export const changeUserStatus = ({ id, type }) => {
  return http({
    url: `users/${id}/state/${type}`,
    method: "put",
  });
};
//修改用户信息
export const editUser = ({ username, id, email }) => {
  return http({
    url: `users/${id}`,
    method: "put",
    data: {
      username,
      email,
    },
  });
};
//删除用户
export const delUser = (id) => {
  return http({
    url: `users/${id}`,
    method: "delete",
  });
};
//查询用户信息
export const getUser = (id) => {
  return http({
    url: `users/${id}`,
    method: "get",
  });
};
//获取角色列表
export const getRolesList = () => {
  return http({
    url: `roles`,
    method: "get",
  });
};
//分配角色
export const distributeRole = ({ id, rid }) => {
  return http({
    url: `users/${id}/role`,
    method: "put",
    data: { rid },
  });
};
//添加角色
export const addRole = (data) => {
  return http({
    url: "roles",
    method: "post",
    data,
  });
};
//编辑角色
export const editRole = ({ id, roleName, roleDesc }) => {
  return http({
    url: `roles/${id}`,
    method: "put",
    data: {
      roleName,
      roleDesc,
    },
  });
};
//删除角色
export const delRole = (id) => {
  return http({
    url: `roles/${id}`,
    method: "delete",
  });
};
//删除角色指定权限
export const delRoleRight = ({ roleId, rightId }) => {
  return http({
    url: `roles/${roleId}/rights/${rightId}`,
    method: "delete",
  });
};
//获取所有权限
export const getAllRights = (type = "tree") => {
  return http({
    url: `rights/${type}`,
    method: "GET",
  });
};
//角色授权
export const divRights = ({ rid, rids }) => {
  return http({
    url: `roles/${rid}/rights`,
    method: "POST",
    data: {
      rids,
    },
  });
};
//获取商品列表
export const getGoodsList = (params) =>
  http({
    url: "goods",
    method: "get",
    params,
  });
//删除商品
export const delGoods = (id) => {
  return http({
    url: `goods/${id}`,
    method: "delete",
  });
};
//获取商品分类

export const getGoodsCategories = () => {
  return http({
    url: `categories`,
    method: "get",
    params: {
      type: 3,
    },
  });
};
//根据分类id查询，商品参数信息

export const getGoodsParamsById = ({ id, sel = "many" }) => {
  return http({
    url: `categories/${id}/attributes`,
    method: "get",
    params: {
      sel,
    },
  });
};
//获取商品
export const getGoodsById = (id) =>
  http({
    url: `goods/${id}`,
    method: "get",
  });

//添加商品
export const addGoods = (data) =>
  http({
    url: `goods`,
    method: "post",
    data,
  });
//编辑商品
export const editGoods = (data) =>
  http({
    url: `goods/${data.goods_id}`,
    method: "put",
    data,
  });

//获取数据
export const getReports = () =>
  http({
    url: `reports/type/1`,
    method: "get",
  });
//添加属性
export const addParams = (data) => {
  return http({
    url: `categories/${data.id}/attributes`,
    method: "POST",
    data,
  });
};

//编辑属性
export const editParams = (data) => {
  return http({
    url: `categories/${data.cat_id}/attributes/${data.attr_id}`,
    method: "put",
    data,
  });
};
//删除属性
export const delParams = ({ id, attr_id }) => {
  return http({
    url: `categories/${id}/attributes/${attr_id}`,
    method: "delete",
  });
};
