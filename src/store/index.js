import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
import { SET_TOKEN, SET_USERNAME } from "./mutation-type";
export default new Vuex.Store({
  state: {
    token: localStorage.getItem("token") ?? "",
    username: localStorage.getItem("username") ?? "",
  },
  mutations: {
    [SET_TOKEN](state, token = "") {
      state.token = token;
      localStorage.setItem("token", token);
    },
    [SET_USERNAME](state, username = "") {
      state.username = username;
      localStorage.setItem("username", username);
    },
  },
  modules: {},
});
