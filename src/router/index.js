import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import store from "../store";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    children: [
      {
        path: "users",
        name: "Users",
        component: () => import("@/views/Users/Index.vue"),
      },
      {
        path: "roles",
        name: "Roles",
        component: () => import("@/views/Roles/Index.vue"),
      },
      {
        path: "rights",
        name: "Rights",
        component: () => import("@/views/Rights/Index.vue"),
      },
      {
        path: "goods",
        component: () => import("@/views/Goods/Index.vue"),
        children: [
          {
            path: "",
            name: "GoodsLists",
            component: () => import("@/views/Goods/GoodsLists.vue"),
          },
          {
            path: "add",
            name: "GoodsAdd",
            component: () => import("@/views/Goods/Add.vue"),
          },
          {
            path: "edit/:id",
            name: "GoodsEdit",
            props: true,
            component: () => import("@/views/Goods/Edit.vue"),
          },
        ],
      },
      {
        path: "reports",
        name: "Reports",
        component: () => import("@/views/Reports/Index.vue"),
      },
      {
        path: "params",
        name: "Params",
        component: () => import("@/views/Params/Index.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login/Index.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: "/vue-admin/",
  routes,
});
router.beforeEach((to, from, next) => {
  if (to.path === "/login") {
    next();
  } else {
    if (store.state.token) {
      next();
    } else {
      next("/login");
    }
  }
});

export default router;
