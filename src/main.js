import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./utils/reset.css";
Vue.config.productionTip = false;
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import http from "./http";
import axios from "./http/http";
import BreadCrumb from "@/components/BreadCrumb";
import fitersObj from "@/utils/filters";
Vue.use(ElementUI, { size: "small" });
import ECharts from "vue-echarts";
import { use } from "echarts/core";
import { CanvasRenderer } from "echarts/renderers";
import { LineChart } from "echarts/charts";
import {
  GridComponent,
  TooltipComponent,
  LegendComponent,
} from "echarts/components";
use([
  CanvasRenderer,
  LineChart,
  GridComponent,
  TooltipComponent,
  LegendComponent,
]);
Vue.component("v-chart", ECharts);
Vue.prototype.$axios = http;
Vue.prototype.axios = axios;
Vue.component("BreadCrumb", BreadCrumb);
for (let i in fitersObj) {
  Vue.filter(i, fitersObj[i]);
}
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
