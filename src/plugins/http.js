import axios from "axios";
const instanceObj = {};

instanceObj.install = function (Vue) {
  Vue.prototype.axios = axios;
};

export default instanceObj;
