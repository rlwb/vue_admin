## 项目介绍

- 后台管理系统
  CMS(内容管理系统)

立项=》收集需求=》产品（设计原型）=》UI，UE（UI 图，psd，sketch，figma）=》前端=》后台（提供 API，接口文档）=》测试（测试，前后端功能）=》运维（上线，部署到服务器）

## 前期准备

- 清除项目无用内容
- vuejs vuerouter vuex axios elementui
- 对于公司项目来说，必须要使用 git 进行版本控制，一般公司分支的策略：
  远程：master（线上），dev（开发），test（测试），本地：feature\_\*（特征分支）,fixed_101（修复 bug）
- 项目开始去除默认样式（reset-css || normalize.css）

* 安装 elementui
* 安装 css 预处理器，注意版本

## 登录模块

- 登录页面参考样式
  1、https://panjiachen.github.io/vue-element-admin/
  2、https://adminpro.iviewui.com/login
  3、https://iczer.gitee.io/vue-antd-admin/#/login
- axios 封装
- [登录机制 JWT](https://ruanyifeng.com/blog/2018/07/json_web_token-tutorial.html)
- 权限管理
- 退出登录
  1、清除一下本地的登录信息（token，username），然后跳转到登录页
  2、调用退出接口，后台返回退出成功之后，跳转登录页就行

- 使用拦截器对 axios 封装
- 通过模式设置环境变量，动态切换 baseUrl

## 后台管理图表库

1、echarts (百度公司出的，apache 基金会)
2、highcharts （国外公司，付费）
3、chart.js
4、G2

### 存在问题

- 1、看不懂报错
  - 解决方法：结合语义翻译过来，对照 js 6 中报错，判断报错原因
  - 写代码别让语法出错，自然不会有报错
  - 搜索
- 2、elementui 结构写不明白，字段不知道怎么修改
  — 读懂 elementui 文档，看示例，比葫芦画瓢
- 3、封装不好
  - 重复处理的问题，考虑封装，统一处理
  - 函数式
  - class

— 4、样式问题

— 5、不知道为啥报错，不知道怎么解决

— 6、封装不会

— 7、封装不会

- 8、侧边栏菜单跳转问题

- 9、封装不会

vue watch 监听，只能监听一个 data
如果同时监听多个 data 的话，可以通过计算属性返回的形式：

```
computed:{
  obj(){
    return [a,b,c,d,e,f....]
  }
}
watch:{
  obj:{
    deep: true,
    handler(){
      .......
    }
  }
}
```

## 清楚业务

80% 思考， 20%

1、最后会出错，白写
2、花费了太多时间，也能产生结果，做了很多无用功

业务为主，多花时间去理解业务。

关注代码层面，

实现它，想方设法去实现就可以

刚去公司，多看

## nodejs mysql 项目部署 Ubuntu 服务器

- 安装 nodejs

```
sudo apt-get nodejs npm
```

- 升级 nodejs 为最新稳定版本

```
sudo npm i -g n

sudo n latest

sudo n lts
```

- filezilla 上传 nodejs 项目代码到服务器（上传前删除掉 node_modules）

- npm 安装下项目依赖

- ubuntu 安装 mysql

```
sudo apt-get install mysql-server

```

- 安装插件

```
mysql_secure_installation
```

> 安装提示

root@ubuntu-virtual-machine:~# mysql_secure_installation

Securing the MySQL server deployment.

Connecting to MySQL using a blank password.

VALIDATE PASSWORD PLUGIN can be used to test passwords
and improve security. It checks the strength of password
and allows the users to set only those passwords which are
secure enough. Would you like to setup VALIDATE PASSWORD plugin? # 要安装验证密码插件吗?

Press y|Y for Yes, any other key for No: N # 这里我选择 N
Please set the password for root here.

New password: # 输入要为 root 管理员设置的数据库密码

Re-enter new password: # 再次输入密码

By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : y # 删除匿名账户
Success.

Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : N # 禁止 root 管理员从远程登录，这里我没有禁止

... skipping.
By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.

Remove test database and access to it? (Press y|Y for Yes, any other key for No) : y # 删除 test 数据库并取消对它的访问权限

- Dropping test database...
  Success.

- Removing privileges on test database...
  Success.

Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : y # 刷新授权表，让初始化后的设定立即生效
Success.

All done!

- 登录 mysql（回车，无需密码）

```
mysql -u root -p
```

- 登录之后,创建数据库，导入数据

```
create database vue_admin;

use vue_admin;

source 后边跟你的.sql文件的路径
```

- 修改加密方式

```
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '和你的default.json里面的password一致的密码';

FLUSH PRIVILEGES;
```

- 启动 api-server 项目

```
node app
```

- 长久启动接口使用 pm2

```
npm install -g pm2
pm2 start app.js
```

## nginx 代理跨域

修改 /etc/nginx/sites-enable/default

```
location /api/ {
        proxy_pass http://localhost:8888;
   }
```

## 项目打包部署白屏问题

```
module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "" : "/",
};
```

nginx 也对修改

```
location /vue-admin {
  try_files $uri $uri/ /vue-admin/index.html;
}
```

## 项目总结

1、登陆如何实现的
2、权限如何做的
3、项目有什么难点
4、项目有什么亮点
5、介绍一下你的 XX 项目
